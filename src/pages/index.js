import React from 'react';
import Game from '@soul/game/game';
import Layout from '@soul/layouts/index';

const IndexPage = () => (
  <Layout>
    <Game />
  </Layout>
)

export default IndexPage;
