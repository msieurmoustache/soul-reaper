import React, { useContext } from 'react';

import GameStateContext from "@soul/contexts/gameStateContext";

import Button from '@soul/components/Button/button';

import UpgradePanel from './upgrades/upgradePanel';
import ScorePanel from './score/scorePanel';
import Reaping from './reaping/reaping';
import Inferno from './inferno/inferno';

import "./game.css";

const defaultState = {souls:0, soulsPerClick:1, humans: 2, upgrades: {}};

const Game = ({}) => {
    const {gameState, setGameState} = useContext(GameStateContext);

    return (
        <div>
            {gameState.humans <= 0 && (
                <div>
                    <h1>
                        Humanity is dead.
                    </h1>
                    <Button onClick={() => setGameState(defaultState)}>
                        Try Again
                    </Button>
                </div>
            )}
            <div className="upper-game-container">
                <ScorePanel />
                <Reaping />   
                <UpgradePanel />  
            </div>
            <div className="game-container">
                <Inferno />
            </div>

        </div>
    );
};

export default Game;