import React, { useContext } from 'react';
import GameStateContext from '@soul/contexts/gameStateContext';

import Button from '@soul/components/Button/button';
import Number from '@soul/components/Number/number';
import Tooltip from '@soul/components/Tooltip/tooltip';

import Circles from './circles';

import "./circles.css";

const CircleRow = ({circle}) => {    
    const { gameState, setGameState } = useContext(GameStateContext);
    const { upgrades, souls, soulsPerSecond } = gameState;
    const upgradeValue = upgrades[circle.toLocaleLowerCase()];
    const baseCost = Math.pow(2,Circles.indexOf(circle));

    function getCost() {
        return Math.ceil(40*baseCost*Math.pow(1.5, upgradeValue));
    }

    function canBuy() {
        var cost = getCost();

        return Boolean(souls >= cost && souls > 0);
    }

    const upgrade = async() => {
        var cost = getCost();
        if (Boolean(souls >= cost && souls > 0)) {
            var newState = {...gameState, 
                souls: souls-cost,
                soulsPerSecond: soulsPerSecond ? soulsPerSecond + baseCost : baseCost
            };
            
            newState.upgrades[circle.toLocaleLowerCase()] = upgradeValue+1;
            await setGameState(newState);
        }
    };
    
    const downgrade = async() => {
        var cost = getCost();
        if (Boolean(souls >= cost && souls > 0)) {
            var newState = {...gameState,
                soulsPerSecond: soulsPerSecond - baseCost
            };
            
            newState.upgrades[circle.toLocaleLowerCase()] = upgradeValue-1;
            await setGameState(newState);
        }
    };

    return (
        <div className={`circle-row ${circle.toLocaleLowerCase()}`}>
            <div className="circle-subcontainer">
                <div className="circle-base-text">
                    <div>                
                    {circle} [{upgradeValue}]  
                    </div>
                </div>
                <Tooltip text={
                    <div>
                        <div>
                            Each unit adds {<Number number={baseCost} />} souls per second
                        </div>
                        <div>
                            <b>Cost: </b>
                            <span><Number number={getCost()} /></span>
                        </div>
                    </div>}>
                    <Button className="circle-add-remove-button" onClick={() => upgrade()} disabled={!canBuy()}>
                        +
                    </Button>    
                </Tooltip>        
                <Button className="circle-add-remove-button" disable={upgradeValue <= 1} onClick={() => downgrade()}>
                    -
                </Button>
            </div>
        </div>
    );
};

export default CircleRow;