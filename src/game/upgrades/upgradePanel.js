import React, { useContext } from 'react';

import GameStateContext from '@soul/contexts/gameStateContext';

import Upgrade from './upgrade';

import HumanityStore from './humanity/humanityStore';

import "./upgrade.css"

const UpgradePanel = () => {
    const { gameState, setGameState } = useContext(GameStateContext);
    const { souls, soulsPerClick, soulsPerSecond, upgrades } = gameState;
    var scythe = upgrades ? (upgrades.scythe ? upgrades.scythe : 1) : 1;
    var limbo = upgrades?.limbo;

    const upgradeScythe = async() => {
        var cost = Math.ceil(5*Math.pow(1.2, scythe)); 
        if (souls >= cost && souls > 0) {
            await setGameState({...gameState, 
                souls: souls-cost, 
                soulsPerClick: soulsPerClick+1, 
                upgrades: {...upgrades, scythe: scythe+1}});
        }
    };

    const unlockCircle = async(circle, cost, baseSouls) => {
        if (souls >= cost && souls > 0) {
            var newState = {...gameState, 
                souls: souls-cost,
                soulsPerSecond: soulsPerSecond ? soulsPerSecond + baseSouls : baseSouls
            };
            
            newState.upgrades[circle.toLocaleLowerCase()] = 1;
            await setGameState(newState);
        }
    };

    return (
        <div className="upgrade-panel">
            <div className="upgrade-panel-container soul-store">
                <b>Soul store</b>
                <Upgrade 
                    name={`Upgrade Scythe ${scythe}`} 
                    cost={Math.ceil(5*Math.pow(1.2, scythe))} 
                    unlockFunction={upgradeScythe} 
                    disabled={souls < Math.ceil(5*Math.pow(1.2, scythe))} />
                {(scythe >= 10 && !limbo) &&
                (
                    <Upgrade 
                        name={`Limbo`} 
                        cost={50} 
                        unlockFunction={() => unlockCircle("Limbo", 50, 1)}
                        disabled={souls < 50} />
                )
                }
                {(limbo >= 10 && !upgrades?.lust) &&
                (                    
                    <Upgrade 
                        name={`Lust`} 
                        cost={100} 
                        unlockFunction={() => unlockCircle("Lust", 100, 2)}
                        disabled={souls < 100} />
                )
                }
                {(upgrades?.lust >= 10 && !upgrades?.gluttony) &&
                (
                    <Upgrade 
                        name={`Gluttony`} 
                        cost={200} 
                        unlockFunction={() => unlockCircle("Gluttony", 200, 4)} 
                        disabled={souls < 200} />
                )
                }
                {(upgrades?.gluttony >= 10 && !upgrades["avarice and prodigality"]) &&
                (
                    <Upgrade 
                        name={`Avarice and prodigality`} 
                        cost={400} 
                        unlockFunction={() => unlockCircle("Avarice and prodigality", 400, 8)} 
                        disabled={souls < 400} />
                )
                }
                {(upgrades["avarice and prodigality"] >= 10 && !upgrades["wrath and sullenness"]) &&
                (                    
                    <Upgrade 
                        name={`Wrath and sullenness`} 
                        cost={800} 
                        unlockFunction={() => unlockCircle("Wrath and sullenness", 800, 15)} 
                        disabled={souls < 800} />
                )
                }
                {(upgrades["wrath and sullenness"] >= 10 && !upgrades["heresy"]) &&
                (                    
                    <Upgrade 
                        name={`Heresy`} 
                        cost={1500} 
                        unlockFunction={() => unlockCircle("Heresy", 1500, 30)} 
                        disabled={souls < 1500} />
                )
                }                
                {(upgrades["heresy"] >= 10 && !upgrades["violence"]) &&
                (                    
                    <Upgrade 
                        name={`Violence`} 
                        cost={3000} 
                        unlockFunction={() => unlockCircle("Violence", 3000, 60)} 
                        disabled={souls < 3000} />
                )
                }                
                {(upgrades["violence"] >= 10 && !upgrades["fraud"]) &&
                (
                    <Upgrade 
                        name={`Fraud`} 
                        cost={5000} 
                        unlockFunction={() => unlockCircle("Fraud", 5000, 120)} 
                        disabled={souls < 5000} />
                )
                }              
                {(upgrades["fraud"] >= 10 && !upgrades["treachery"]) &&
                (
                    <Upgrade 
                        name={`Treachery`} 
                        cost={10000} 
                        unlockFunction={() => unlockCircle("Treachery", 10000, 250)} 
                        disabled={souls < 10000} />
                )
                }               
            </div>
            <HumanityStore className="upgrade-panel-container" />
        </div>
    );
};

export default UpgradePanel;