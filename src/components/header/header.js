import React, { useState } from 'react';

import Button from '@soul/components/Button/button';
import Modal from '@soul/components/modal/Modal';

import Settings from '@soul/game/settings/settings';

import Cog from "@soul/assets/cog.png";

import "./header.css";

const Header = () => {
    const [open, setOpen] = useState(false);
    
    return (
        <header>          
            <Button className="settings-button" onClick={() => setOpen(!open)}>
                <img src={Cog} alt="cog-game-settings" />
            </Button>
            <Modal open={open} onClose={() => setOpen(false)}>
                <Settings />
            </Modal>
        </header>
    );
}

export default Header;