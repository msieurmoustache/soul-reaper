import React from "react";
import "./tooltip.css";

const Tooltip = ({text, children}) => {
    if(!text) {
        return children;
    }

    return (
        <div class="tooltip">
            {children}
            <span class="tooltiptext">{text}</span>
        </div>
    )
};

export default Tooltip;