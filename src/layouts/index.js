import React, { useState, useEffect } from 'react';
import Helmet from 'react-helmet';

import GameStateContext from "@soul/contexts/gameStateContext";

import Header from '../components/header/header';

import './index.css';

const defaultState = {souls:0, soulsPerClick:1, humans: 2, upgrades: {}};

const Layout = ({ children }) => {
  const [gameState, setGameState] = useState(defaultState);
    const [shouldSave, setShouldSave] = useState(false);    

    useEffect(() => {
        if(shouldSave) {
            localStorage.setItem("soulReaperSave", JSON.stringify(gameState));
            setShouldSave(false);
        }
    }, [shouldSave]);

    useEffect(() => {
        setInterval(() => {
            console.log("saving state");
            setShouldSave(true);
        }, 20000);
      }, []);

    useEffect(() => {    
        const gameState = JSON.parse(localStorage.getItem("soulReaperSave"));
        console.log(gameState);
        if(gameState) {
            setGameState(gameState);
        }
    }, []);

    return (
      <GameStateContext.Provider value={{gameState, setGameState}}>
        <Helmet
          title="Soul Reaper"
          meta={[
            { name: 'description', content: "Soul Reaper is an incremental game set around Dante's Inferno" },
            { name: 'keywords', content: 'incremental,game,soul,reaper' },
          ]}
        />
        <Header />
        <main>
          {children}
        </main>
      </GameStateContext.Provider>
      );


}

export default Layout;
