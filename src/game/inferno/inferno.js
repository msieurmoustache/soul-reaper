import React, { useContext } from 'react';
import GameStateContext from '@soul/contexts/gameStateContext';

import Circles from './circles/circles';
import CircleRow from './circles/CircleRow';

import "./inferno.css";

const Inferno = ({}) => {
    const { gameState } = useContext(GameStateContext);
    const { upgrades } = gameState;

    
    return (
        <div className="inferno-container">
            <b>Inferno</b>
            {Circles.map((x, i) => {
                if(!upgrades[x.toLocaleLowerCase()]) {
                    return null;
                }

                return <CircleRow circle={x} />;
            })}
        </div>
    );
};

export default Inferno;