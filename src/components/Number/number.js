import React from "react";

const Number = ({number, className}) => {
    const renderedNumber = number > 100000 ? number.toExponential(2) : number;

    return (
        <span className={`number ${className}`}>
            {renderedNumber}
        </span>
    );
};

export default Number;